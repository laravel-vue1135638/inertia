<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\resourceData>
 */
class resourceDataFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name,
            'nickname' => fake()->name,
            'katakanaName' => fake()->name,
            'resume' => fake()->name,
            'dateHired' => fake()->date,
            'rate' => fake()->numberBetween(1, 50000),
            'ibmRate' => fake()->numberBetween(1, 50000),
            'ubicomRate' => fake()->numberBetween(1, 50000),
            'poUntil' => fake()->dateTime,
            'actionBatch' => fake()->word,
            'yoePreAws' => fake()->numberBetween(0, 10),
            'yoe' => fake()->numberBetween(0, 10),
            'techCertification' => fake()->word,
            'coreSkills' => fake()->word,
            'subSkill5' => fake()->word,
            'subSkill4' => fake()->word,
            'subSkill3' => fake()->word,
            'subSkill2' => fake()->word,
            'subSkill1' => fake()->word,
            'nihongo' => fake()->word,
            'position' => fake()->word,
            'buOff' => fake()->word,
            'buAss' => fake()->word,
            'project' => fake()->word,
            'currentAssignment' => fake()->word,
            'candidateFor' => fake()->word,
            'loc' => fake()->word,
            'email' => fake()->email,
            'mainSkill' => fake()->word,
            'subSkill' => fake()->word,
            'focusSkill' => fake()->word,
            'location1' => fake()->word,
            'location2' => fake()->word,
            'contactNumber' => fake()->phoneNumber,
            'passportExpiration' => fake()->date,
            'remarks' => fake()->word,
        ];
    }
}
