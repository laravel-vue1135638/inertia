<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('resource_data', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('nickname');
            $table->string('katakanaName');
            $table->string('resume');
            $table->dateTime('dateHired');
            $table->float('rate');
            $table->float('ibmRate');
            $table->float('ubicomRate');
            $table->dateTime('poUntil');
            $table->string('actionBatch')->nullable();
            $table->string('yoePreAws')->nullable();
            $table->string('yoe');
            $table->string('techCertification')->nullable();
            $table->string('coreSkills')->nullable();
            $table->string('subSkill5')->nullable();
            $table->string('subSkill4')->nullable();
            $table->string('subSkill3')->nullable();
            $table->string('subSkill2')->nullable();
            $table->string('subSkill1')->nullable();
            $table->string('nihongo')->nullable();
            $table->string('position');
            $table->string('buOff');
            $table->string('buAss');
            $table->string('project');
            $table->string('currentAssignment');
            $table->string('candidateFor');
            $table->string('loc');
            $table->string('email');
            $table->string('skill')->nullable();
            $table->string('subSkill')->nullable();
            $table->string('focusSkill')->nullable();
            $table->string('location1');
            $table->string('location2');
            $table->string('contactNumber');
            $table->string('passportExpiration');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('resource_data');
    }
};
