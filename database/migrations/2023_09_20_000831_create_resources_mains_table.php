<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('resources_mains', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('nickname');
            $table->string('katakanaName');
            $table->string('resume');
            $table->dateTime('dateHired');
            $table->float('rate');
            $table->float('ibmRate');
            $table->float('ubicomRate');
            $table->dateTime('poUntil');
            $table->string('actionBatch');
            $table->string('yoePreAws)');
            $table->string('yoe');
            $table->string('techCertification');
            $table->string('coreSkills');
            $table->string('subSkill5');
            $table->string('subSkill4');
            $table->string('subSkill3');
            $table->string('subSkill2');
            $table->string('subSkill1');
            $table->string('nihongo');
            $table->string('position');
            $table->string('buOff');
            $table->string('buAss');
            $table->string('project');
            $table->string('currentAssignment');
            $table->string('candidateFor');
            $table->string('loc');
            $table->string('email');
            $table->string('mainSkill');
            $table->string('subSkill');
            $table->string('focusSkill');
            $table->string('location1');
            $table->string('location2');
            $table->string('contactNumber');
            $table->string('passportExpiration');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('resources_mains', ['name','nickname','katakana_name','resume','date_hired','rate','ibm_rate','ubicom_rate','po_Until','action_batch','yoe(pre-aws)','yoe','tech_certification','core_skills','sub_skill5','sub_skill4','sub_skill3','sub_skill2','sub_skill1','nihongo','position','bu_off','bu_ass','project','current_assignment','candidate_for','loc','email','main_Skill','sub_Skill','focus_Skill','focus_Skill','location1','location2','contact#','passport_Expiration','remarks']);
    }
};
