<?php

use App\Http\Controllers\IndexController;
use App\Http\Controllers\ResourceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [IndexController::class, 'index']);
Route::get('/show', [IndexController::class, 'show']);

Route::get('overview', [ResourceController::class, 'index'])->name('overview.index');

Route::get('create', [ResourceController::class, 'create']);

Route::post('overview', function(){
            // validate the request
            $attributes = Request::validate([
                'name' => 'required',
                'nickname' => 'required',
                'カタカナ名' => 'required',
                'resume' => 'required'
            ]);

            // create user
            overview::create($attributes);

            // redirect


});
