<?php

namespace App\Http\Controllers;

use App\Models\resourceData;
use Illuminate\Http\Request;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return inertia('Index/TableOverview', [
            'resource'=>resourceData::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia('Components/CreateUser');
    }

    public function add()
    {
        // // validate the request
        // Request::validate([
        //     'name' => 'required',
        //     'nickname' => 'required',
        //     'カタカナ名' => 'required',
        //     'resume' => 'required'
        // ]);

        // // create

    }

    // public function addUser()
    // {
    //     $attributes=Request::validate([
    //         'Name' => 'required'
    //     ]);
    //     // Create user
    //     resourceData::create($attributes);

    //     // redirect
    //     return redirect('overview')
    // }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
